<?php

if (!$_SERVER["REMOTE_USER"]) {
  echo "Not Allowed";
  exit();
 }

require_once("config.php");

require_once("head.php");

if (count($error)) {
  echo "<div class=\"error\">";
  foreach($error as $e) {
    echo $e."<br />";
  }
  echo "</div>";
 }
if (count($info)) {
  echo "<div class=\"info\">";
  foreach($info as $e) {
    echo $e."<br />";
  }
  echo "</div>";
 }

?>

<table><tr><td>

<h2>Ajout de tag</h2>

<form method="post" action="tag_add.php">
  Entrez les tags � cr�er, un par ligne et choisissez un groupe de rattachement<br />
Les tags d�j� existant dans le groupe seront ignor�s.<br />
<textarea name="newtag" cols="40" rows="10"></textarea>
<br />
<?php
  eradio("taggroup",$_REQUEST["group"],"","group");
?>
<input type="submit" name="go" value="Ajouter"/>
</form>

</td><td style="padding-left: 20px">

<h2>G�rer les groupes de tags</h2>

<table class="formv">
<tr>
<th colspan="3">Action</th>
<th>Groupe</th>
<th>Tags</th>
</tr>

<?php

$r=mysql_query("SELECT tg.*, COUNT(t.id) AS ct FROM taggroup tg, tag t WHERE t.groupid=tg.id GROUP BY tg.id ORDER BY tg.name ;");
$odd="odd";
while ($c=mysql_fetch_array($r)) {
if ($odd=="odd") $odd="even"; else $odd="odd";
  echo "<tr class=\"$odd\">";
  echo "<td><a href=\"taggroup_del.php?id=".$c["id"]."\">Effacer</a></td>";
  echo "<td><a href=\"taggroup_rename.php?id=".$c["id"]."\">Renommer</a></td>";
  echo "<td><a href=\"taggroup_edit.php?id=".$c["id"]."\">Modifier les tags</a></td>";
  echo "<td>".$c["name"]."</a></td>";
  echo "<td>".$c["ct"]."</a></td>";
  echo "</tr>\n";
 }

?>
</table>

</td></tr></table>

<ul>
 <li><a href="/">Retour au Mediakit</a></li>
</ul>


<?php
require_once("foot.php");
?>