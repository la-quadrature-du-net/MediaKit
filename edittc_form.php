<?php

require_once("config.php");

$tid=0; // no current TimeCode

$id=intval($_REQUEST["id"]);

$m=mqone("SELECT * FROM media WHERE id='$id';");

if (!$id || !$m) {
  $error[]="ID de media non fourni ou media non trouv� !";
  require_once("index.php");
  exit();
 }

if ($_REQUEST["go"]) {
  // On ajoute le timecode
  $start=trim($_REQUEST["start"]);
  $end=trim($_REQUEST["end"]);
  if (!$start && !$end) {
    $error[]="Il faut au moins remplir un champ timecode ...";
  } else {
    if (preg_match("/^[0-9]*:[0-9]*$/",$start)) $start="00:".$start;
    if (preg_match("/^[0-9]*:[0-9]*$/",$end)) $end="00:".$end;
    mq("INSERT INTO tc SET media='$id', start='".asl($start)."', end='".asl($end)."', description='".asl(trim($_REQUEST["description"]))."';");
    if ($tid=mysql_insert_id()) {
      $info[]="TC cr�� avec succ�s";
      // Ok, on g�re les tags maintenant
      $groups=mqlistone("SELECT id FROM taggroup;");
      foreach($groups as $gid) {
	if (is_array($_REQUEST["tg".$gid."v"])) {
	  foreach($_REQUEST["tg".$gid."v"] as $tagid) {
	    mq("INSERT INTO tctag SET tc='$tid',tag='$tagid';");
	  }
	  unset($_REQUEST["tg".$gid."v"]);
	}
      }
    } else {
      $error[]="Impossible de cr�er le TC";
    }
  } 
 }

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" dir="ltr">
<head>
    <link rel="icon" href="./favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO8859-1" />
  <script type="text/javascript" src="swfobject.js"></script>
  <script type="text/javascript" src="component_script.js"></script>
  <script type="text/javascript" src="script_common.js"></script>
  <link rel="stylesheet" type="text/css" href="/style.css" />
  <link rel="stylesheet" type="text/css" href="/component_style.css" />
</head>
   <body class="ifr">
<script type="text/javascript">
  window.parent.tclist.document.location='edittc_list.php?id=<?=$id; ?>';
</script>
<?php

if (count($error)) {
  echo "<div class=\"error\">";
  foreach($error as $e) {
    echo $e."<br />";
  }
  echo "</div>";
 }
if (count($info)) {
  echo "<div class=\"info\">";
  foreach($info as $e) {
    echo $e."<br />";
  }
  echo "</div>";
 }

?>
<form method="post" action="edittc_form.php">
<input type="hidden" name="id" value="<?=$id; ?>"/>
<table>
<tr><td colspan="2">
  Ajout d`une note timecod�e sur le media
</td></tr>
<tr><td>
  Description : <input type="text" name="description" value="" style="width: 300px"/><input type="submit" name="go" value="OK"  />
</td></tr>
<tr><td>
  Timecode D�but (mm:ss) <input type="text" name="start" value="" size="6"/> Fin <input type="text" name="end" value="" size="6"/> 
</td></tr>
<tr><td>
   <table>
   <?php
  
  function tagoption($gid) {
  $r=mq("SELECT id,name FROM tag WHERE groupid='$gid';");
  while ($c=mysql_fetch_array($r)) {
    echo "<option value=\"".$c["id"]."\">".$c["name"]."</option>";
  }
}

  $r=mq("SELECT tctag.tag, tag.groupid, tag.name FROM tctag, tag WHERE tctag.tc='$tid' AND tctag.tag=tag.id ORDER BY tag.groupid,tag.name;");
echo mysql_error();
  while ($c=mysql_fetch_array($r)) {
    $curtags[$c["groupid"]][$c["tag"]]=$c["name"];
  }
  $groups=mqassoc("SELECT id,name FROM taggroup ORDER BY name;");
  require_once("component_multiselect.php");

foreach($groups as $gid=>$gname) {
  echo "<tr><th>$gname</th>";
?>
<td valign="top" style="vertical-align: top">
<select class="inl" name="tg<?=$gid ?>l" id="tg<?=$gid ?>l" onKeyDown="return ms_key('tg<?=$gid ?>',event);">
   <?php tagoption($gid); ?>
      </select>&nbsp;&nbsp;<a href="javascript:ms_add('tg<?=$gid ?>')"><img src="add.gif" style="vertical-align: middle" alt="Ajouter ce tag" title="Ajouter ce tag" /></a>
</td>
<td>
<div id="tg<?=$gid ?>d">
<?php
			      ms_divFromArray($curtags[$gid],"tg".$gid);
?>
</div>
<?php
  echo "</td></tr>\n";
}
   ?>
   </table>
</td></tr>
</table>
</form>
</body>
</html>