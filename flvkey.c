/* **********************************************************************
 * 
 * This programm is distributed under GPL License V2.0
 * See COPYRIGHT file for more information.
 * 
 * (C) Benjamin Sonntag <benjamin@sonntag.fr> 20070609
 * 
 * flvkey is a flv Video file keyframe finder. It spits out the offset 
 * where a FLV file starts a new keyframe.
 * It takes a flv file on stdin. 
 * 
 ********************************************************************* */

//  if (DEBUG) fprintf(stderr,"Debug: \n");


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
      
#define DEBUG 0

#define MAX_TAG_SIZE 262144

#define FRAME_VIDEO 0x09
#define FRAME_AUDIO 0x08
#define FRAME_METADATA 0x12

// Characteristics of the stream : 
char hasvideo=0;
char hasaudio=0;

// Converts a ui24 flash to unsigned long
unsigned long ui24_ulong(unsigned char *c) {
  return c[2]+256*(c[1]+256*c[0]);
}
// Converts a ui32 flash to unsigned long
unsigned long ui32_ulong(unsigned char *c) {
  return c[3]+256*(c[2]+256*(c[1]+256*c[0]));
}


int main(int argc,char *argv[]) {
  unsigned char c;
  unsigned char s[MAX_TAG_SIZE]; // Headers & frames cannot be larger than MAX_TAG_SIZE
  FILE *cf;
  FILE *cfmd; // For metadata copy (0.MD)
  char fpath[PATH_MAX+20],temppath[PATH_MAX];

  // vars used by stream copy or append functions : 
  char savepath[PATH_MAX+20];
  unsigned int saveit, appendit, saveisopen, rcopy, tcopy;
  FILE *saveh;
  unsigned char scopy[MAX_TAG_SIZE]; // Headers & frames cannot be larger than MAX_TAG_SIZE

  unsigned long offset;
  unsigned char datasize_s[3];
  unsigned char timestamp_s[4];
  unsigned char streamid_s[4];
  struct stat tstat;

  unsigned long frameno=0,datasize,timestamp;

  if (argc>1) {
    fprintf(stderr,"Usage : %s \n  Spits out (stdout) the stdin-passed flv file keyframe offsets.\n",argv[0]);
    exit(-1);
  }
  
  // Set stdin to unbuffered mode : 
  setvbuf(stdin, 0, _IONBF , 0);
  offset=0;

  // We read 3 Bytes = FLV 
  fread(s, 3,1,stdin);
  s[3]=0;
  offset+=3;
  // FLV
  if (strcmp(s,"FLV")) {
    fprintf(stderr,"Fatal: FLV file not starting with FLV '%s'\n",s);
    exit(-1);
  }
  // Version 1
  c=fgetc(stdin);
  offset+=1;
  if (c!=1) {
    fprintf(stderr,"Fatal: I don't support FLV version other than 1\n");
    exit(-1);
  }
  // Audio, Video, or Both
  c=fgetc(stdin);
  offset+=1;
  hasvideo=(c & 1);
  hasaudio=(c & 4);  
  // We skip 4 bytes = 00 00 00 09 (header size)
  // + 4 bytes = size of previous frame = 0 :) 
  fread(s,8,1,stdin);
  offset+=8;

  if (DEBUG>1) fprintf(stderr,"Debug: starting the main loop\n");
  // ----------------------------------------
  // Main loop : read frames and write them back
  c=fgetc(stdin); // FRAME TYPE 
  offset+=1;

  while (!feof(stdin)) { 
    if (DEBUG>1) fprintf(stderr,"Debug: found frame type %hhX\n",c);

    // Read the data that is common to every frame : 
    fread(datasize_s,3,1,stdin);
    offset+=3;
    datasize=ui24_ulong(datasize_s);
    if (DEBUG>2) fprintf(stderr,"Debug: %hhX %hhX %hhX = %lu\n",datasize_s[0],datasize_s[1],datasize_s[2],datasize);
    fread(timestamp_s,4,1,stdin);
    offset+=4;
    timestamp=ui32_ulong(timestamp_s);
    fread(streamid_s,3,1,stdin); // Skip StreamID (0)
    offset+=3;
    if (DEBUG>2) fprintf(stderr,"Debug: datasize: %lu, timestamp: %lu\n",datasize,timestamp);

    if (datasize > MAX_TAG_SIZE) { 
      fprintf(stderr,"Fatal: Cannot handle frame size bigger than %d (size found is %lu) [offset %lu]\n",MAX_TAG_SIZE,datasize,offset);
      exit(-1);
    }

    // Now, depending on the frame type, do sthg different.
    switch (c) {
    case FRAME_VIDEO:
      // It's a video : if it is a keyframe, let's spit offset out
      // If it's not, let's continue.
      fread(s,datasize,1,stdin);
      offset+=datasize;

      if ((s[0] & 0xF0)==0x10) {
	// It's a keyframe : let's spit ! 
	printf("%lu\n",offset-datasize-11); // 3+4+4
	frameno++;
      }

      fread(s,4,1,stdin); // Previous frame size.
      offset+=4;

      break;
    case FRAME_AUDIO:
      fread(s,datasize,1,stdin);
      offset+=datasize;
      fread(s,4,1,stdin); // Previous frame size.
      offset+=4;

      break;
    case FRAME_METADATA:
      // Read the metadata and put them inside a file named "0.MD"
      fread(s,datasize,1,stdin);
      offset+=datasize;
      fread(s,4,1,stdin); // Previous frame size.
      offset+=4;

      break;
    default: // Other frames are silently ignored.
      if (DEBUG>1) fprintf(stderr,"Debug: ignored frame\n");
      fread(s,datasize,1,stdin); 
      offset+=datasize;
      fread(s,4,1,stdin); // Previous frame size.
      offset+=4;
      break;
    }

    c=fgetc(stdin); // Frame type for next frame.
    offset+=1;
  }

  // Free what has been allocated
}
