<?php

require_once("config.php");

$tid=0; // no current TimeCode

$id=intval($_REQUEST["id"]);

$m=mqone("SELECT * FROM media WHERE id='$id';");

if (!$id || !$m) {
  $error[]="ID de media non fourni ou media non trouvé !";
  require_once("index.php");
  exit();
 }

$r=mq("SELECT * FROM tc WHERE media='$id';");
if (mysql_num_rows($r)==0) {
  $error[]="Aucun TimeCode pour ce media";
 }

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" dir="ltr">
<head>
    <link rel="icon" href="./favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO8859-1" />
  <script type="text/javascript" src="swfobject.js"></script>
  <script type="text/javascript" src="component_script.js"></script>
  <script type="text/javascript" src="script_common.js"></script>
  <link rel="stylesheet" type="text/css" href="/style.css" />
  <link rel="stylesheet" type="text/css" href="/component_style.css" />
</head>
   <body class="ifr">
<?php

if (count($error)) {
  echo "<div class=\"error\">";
  foreach($error as $e) {
    echo $e."<br />";
  }
  echo "</div>";
 }
if (count($info)) {
  echo "<div class=\"info\">";
  foreach($info as $e) {
    echo $e."<br />";
  }
  echo "</div>";
 }

if (mysql_num_rows($r)>0) {

?>

<table class="formv">
<tr>
  <th></th>
  <th>Description</th>
  <th>Start</th>
  <th>End</th>
  <th>Tags</th>
</tr>

<?php

  function timeshort($t) {
  if ($t=="00:00:00") return "";
  if (substr($t,0,3)=="00:") return substr($t,3);
  if (substr($t,0,1)=="0") return substr($t,1);
  return $t;
}

  $odd="odd";
while ($c=mysql_fetch_array($r)) {
  if ($odd=="odd") $odd="even"; else $odd="odd";
  echo "<tr>";
  echo "<td><a href=\"edittc_del.php?id=".$c["id"]."\"><img src=\"del.gif\"/></a></td>";
  echo "<td>".$c["description"]."</td>";
  echo "<td>".timeshort($c["start"])."</td>";
  echo "<td>".timeshort($c["end"])."</td>";
  echo "<td>";
  $tags=mqlistone("SELECT t.name FROM tag t, tctag tc WHERE tc.tag=t.id AND tc.tc='".$c["id"]."'");
  $first=true;
  foreach($tags as $tname) {
    if (!$first) echo ", ";
    echo $tname;
    $first=false;
  }
  echo "</td>";

}
?>
</table>

    <?php } ?>

</body>
</html>