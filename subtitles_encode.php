<?php


require_once("config.php");
require_once("formats.php");


echo "D�but de taitement des SOUSTITRES\n";
$r=mysql_query("SELECT s.id, s.lang, m.filename, s.media, m.vx, m.vy, m.DAR, m.PAR FROM srt s, media m WHERE m.type=".MEDIA_VIDEO." AND m.id=s.media AND s.encoded=0 ORDER BY s.datem ASC;");
while ($c=mysql_fetch_array($r)) {
  $id=$c["id"];
  echo "media ".$c["filename"]." ($id, ".$c["media"]."), ".$c["lang"]."\n";
  // Pour chaque sous-titre, on lance la g�n�ration : 
  mq("UPDATE srt SET encoded=1, datem=NOW() WHERE id='$id';");
  $filename=$c["filename"];

  // Prepare the audio-only :
  exec("rm -rf tmp_srt/*");
  passthru("ffmpeg -i ".escapeshellarg("files/".$filename)." -vn -acodec copy tmp_srt/audio.mkv");
  // Now prepare the 2-pass video with hard subtitles : 
  // -before-subfont-text-scale 3
  passthru("mencoder -nosound -ovc lavc -lavcopts keyint=25:vcodec=mpeg4:vbitrate=8000 -subwidth 100 -subfont-outline 4 -sub srt/$id -subcp UTF8 -o tmp_srt/video.avi ".escapeshellarg("files/".$filename)."");
  // before-subfont-text-scale 3 -subwidth 100 -subfont-outline 4 
  // Is it OK ? if yes, encode to ogg/webm/mp4 : 
  if (!file_exists("tmp_srt/audio.mkv") || !filesize("tmp_srt/audio.mkv") ||
      !file_exists("tmp_srt/video.avi") || !filesize("tmp_srt/video.avi")) {
    // Don't set encoded to 2 : error ;) 
    mq("UPDATE srt SET encoded=3, datem=NOW() WHERE id='$id';");
    continue;
  } 
 
  // Get the ratio : 4:3 or 16:9 
  // And get the max resolution available.   
  list($sx,$sy,$aspect)=getSizeAndAspect($c);
  if ($sx!=$c["vx"] || $sy!=$c["vy"]) {
    $vf=" -vf pad=".$sx.":".$sy." ";
  } else {
    $vf="";
  }
  list($smallx,$smally,$bigx,$bigy)=getEncodeSizes($aspect,$sx,$sy);
  
  $bigb="1900k"; $bigbmax="4000k"; $bigab="128k";
  $smallb="404k"; $smallbmax="1500k"; $smallab="96k";

  // OGG : 

  // BIG encoding : 
  $exec="ffmpeg -y -i tmp_srt/video.avi $vf -r 30000/1001 -s ".$bigx."x".$bigy." -aspect ".$aspect."  -vcodec libtheora -g 15 -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -b ".$bigb." -bt 1024k -maxrate ".$bigbmax." -bufsize 4M -an -pass 1 -an -f ogg formats_srt/18/".$id."_big.ogg";
  echo "LAUNCHING $exec\n";
  passthru($exec,$ret);  
  $exec="ffmpeg -y -i tmp_srt/audio.mkv -i tmp_srt/video.avi $vf -r 30000/1001 -s ".$bigx."x".$bigy." -aspect ".$aspect."  -vcodec libtheora -g 15 -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -b ".$bigb." -bt 1024k -maxrate ".$bigbmax." -bufsize 4M -acodec libvorbis -ac 2 -ab ".$bigab." -y -f ogg formats_srt/18/".$id."_big.ogg";
  echo "LAUNCHING $exec\n";
  passthru($exec,$ret);

  // SMALL encoding : 
  passthru("ffmpeg -y -i tmp_srt/video.avi $vf -r 30000/1001 -s ".$smallx."x".$smally." -aspect ".$aspect."  -vcodec libtheora -g 15 -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -b ".$smallb." -bt 1024k -maxrate ".$smallbmax." -bufsize 4M -an -pass 1 -an -f ogg formats_srt/18/".$id."_small.ogg",$ret);  
  passthru("ffmpeg -y -i tmp_srt/audio.mkv -i tmp_srt/video.avi $vf -r 30000/1001 -s ".$smallx."x".$smally." -aspect ".$aspect."  -vcodec libtheora -g 15 -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -b ".$smallb." -bt 1024k -maxrate ".$smallbmax." -bufsize 4M -acodec libvorbis -ac 2 -ab ".$smallab." -y -f ogg formats_srt/18/".$id."_small.ogg",$ret);
  
  // WEBM : 
  
  passthru("ffmpeg -y -i tmp_srt/video.avi $vf -s ".$bigx."x".$bigy." -aspect ".$aspect." -vpre libvpx-720p -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -b ".$bigb." -bt 1024k -maxrate ".$bigbmax." -bufsize 4M -pass 1 -an -f webm -y formats_srt/19/".$id."_big.webm",$ret);  
  passthru("ffmpeg -y -i tmp_srt/audio.mkv $vf -i tmp_srt/video.avi -s ".$bigx."x".$bigy." -aspect ".$aspect." -vpre libvpx-720p -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -b ".$bigb." -bt 1024k -maxrate ".$bigbmax." -bufsize 4M -pass 2 -acodec libvorbis -ab ".$bigab." -ac 2 -f webm -y formats_srt/19/".$id."_big.webm",$ret);
  
  // SMALL encoding : 
  passthru("ffmpeg -y -i tmp_srt/video.avi $vf -s ".$smallx."x".$smally." -aspect ".$aspect." -vpre libvpx-720p -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -b ".$smallb." -bt 1024k -maxrate ".$smallbmax." -bufsize 4M -pass 1 -an -f webm -y formats_srt/19/".$id."_small.webm",$ret);  
  passthru("ffmpeg -y -i tmp_srt/audio.mkv $vf -i tmp_srt/video.avi -s ".$smallx."x".$smally." -aspect ".$aspect." -vpre libvpx-720p -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -b ".$smallb." -bt 1024k -maxrate ".$smallbmax." -bufsize 4M -pass 2 -acodec libvorbis -ab ".$smallab." -ac 2 -f webm -y formats_srt/19/".$id."_small.webm",$ret);
  
  // BIG encoding
  passthru("ffmpeg -y -i tmp_srt/video.avi $vf -r 30000/1001 -s ".$bigx."x".$bigy." -aspect ".$aspect." -vcodec libx264 -b ".$bigb." -bt 1024k -maxrate ".$bigbmax." -flags +loop -cmp +chroma -me_range 16 -g 300 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -rc_eq \"blurCplx^(1-qComp)\" -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -coder 0 -refs 1 -bufsize 4M -level 21 -partitions parti4x4+partp8x8+partb8x8 -subq 5 -f mp4 -pass 1 -an formats_srt/20/".$id."_big.tmp",$ret);  
  passthru("ffmpeg -y -i tmp_srt/audio.mkv -i tmp_srt/video.avi $vf -r 30000/1001 -s ".$bigx."x".$bigy." -aspect ".$aspect." -vcodec libx264 -b ".$bigb." -bt 1024k -maxrate ".$bigbmax." -flags +loop -cmp +chroma -me_range 16 -g 300 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -rc_eq \"blurCplx^(1-qComp)\" -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -coder 0 -refs 1 -bufsize 4M -level 21 -partitions parti4x4+partp8x8+partb8x8 -subq 5 -f mp4 -pass 2 -acodec libfaac -ac 2 -ar 44100 -ab ".$bigab." formats_srt/20/".$id."_big.tmp",$ret);
  passthru("qt-faststart formats_srt/20/".$id."_big.tmp formats_srt/20/".$id."_big.mp4 2>&1");
  passthru("rm -f formats_srt/20/".$id."_big.tmp 2>&1");
  
  // SMALL encoding
  passthru("ffmpeg -y -i tmp_srt/video.avi $vf -r 30000/1001 -s ".$smallx."x".$smally." -aspect ".$aspect." -vcodec libx264 -b ".$smallb." -bt 1024k -maxrate ".$smallbmax." -flags +loop -cmp +chroma -me_range 16 -g 300 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -rc_eq \"blurCplx^(1-qComp)\" -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -coder 0 -refs 1 -bufsize 4M -level 21 -partitions parti4x4+partp8x8+partb8x8 -subq 5 -f mp4 -pass 1 -an formats_srt/20/".$id."_small.tmp",$ret);  
  passthru("ffmpeg -y -i tmp_srt/audio.mkv -i tmp_srt/video.avi $vf -r 30000/1001 -s ".$smallx."x".$smally." -aspect ".$aspect." -vcodec libx264 -b ".$smallb." -bt 1024k -maxrate ".$smallbmax." -flags +loop -cmp +chroma -me_range 16 -g 300 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -rc_eq \"blurCplx^(1-qComp)\" -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -coder 0 -refs 1 -bufsize 4M -level 21 -partitions parti4x4+partp8x8+partb8x8 -subq 5 -f mp4 -pass 2 -acodec libfaac -ac 2 -ar 44100 -ab ".$smallab." formats_srt/20/".$id."_small.tmp",$ret);
  passthru("qt-faststart formats_srt/20/".$id."_small.tmp formats_srt/20/".$id."_small.mp4 2>&1");
  passthru("rm -f formats_srt/20/".$id."_small.tmp 2>&1");
  
  
  
  mq("UPDATE srt SET encoded=2, datem=NOW() WHERE id='".$c["id"]."';");
  
}
echo "Fin de taitement des SOUSTITRES\n";


?>