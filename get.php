<?php

require_once("config.php");

$id=intval($_REQUEST["id"]);
$format=intval($_REQUEST["format"]);

if ($id && $format) {
  require_once("formats.php");
  downloadFormat($id,$format);
  exit();
 }

if ($_REQUEST["action"]=="extractdv" && $id) {
  require_once("formats.php");
  downloadDV($id,intval($_REQUEST["minstart"])*60+intval($_REQUEST["secstart"]),intval($_REQUEST["minstop"])*60+intval($_REQUEST["secstop"]));
  exit();
 }

if (!$_REQUEST["id"]) {
  require_once("head.php");
  echo "<div class=\"error\">ID de media non pr�cis� !</div>";
  require_once("foot.php");
  exit();
 }
$me=@mysql_fetch_array(mysql_query("SELECT * FROM media WHERE id='".asl($id)."';"));
if (!$me) {
  require_once("head.php");
  echo "<div class=\"error\">Media $id non trouv� !</div>";
  require_once("foot.php");
  exit();
 }

require_once("head.php");
?>

<div  style="position: fixed; top: 30px; right: 30px; border: 2px inset blue; padding: 3px;" >
<p style="padding:0;margin:0"><a href="javascript:swapmonitor();" alt="Affiche / Masque le moniteur (alt-shift-h)" title="Affiche / Masque le moniteur (alt-shift-h)" accesskey="h">+/- moniteur</a></p>
<iframe id="view" name="view" style="width: 420px; height: 420px; border: 0px; margin: 0; padding: 0; <?php 
if ($_COOKIE['showmonitor']==0) {
echo "display: none;";
}
?>" src="view.php?id=<?=$id; ?>"></iframe>
<script type="text/javascript">
 <?php if ($_COOKIE['showmonitor']!=0) {
?>
  document.getElementById('view').visibility="visible";
<?php 
}
?>

function swapmonitor() {
  s='view';
     if (document.all) {
     if (document.all[s]) {
       if (document.all[s].visibility=="visible") {
	 document.cookie = "showmonitor=0";
       	 hide(s);
       } else {
	 document.cookie = "showmonitor=1";
         show(s,'block');
       }
     }
   } else {
     if (document.getElementById(s)) {
       if (document.getElementById(s).visibility=="visible") {
	 document.cookie = "showmonitor=0";
       	 hide(s);
       } else {
	 document.cookie = "showmonitor=1";
         show(s,'block');
       }
     }
   }
}
function showmonitor() {
  s='view';
  if (document.all) {
    if (document.all[s]) {
      document.cookie = "showmonitor=1";
      show(s,'block');
    }
  } else {
    if (document.getElementById(s)) {
      document.cookie = "showmonitor=1";
      show(s,'block');
    }
  }
}
</script>
</div>



<h1><?php echo $me["filename"]; ?></h1>
<p><?php echo $me["description"]; ?></p>
<p>Type de media : <b><?php echo $amedia[$me["type"]]; ?></b></p>
<ul>
<?php 

$r=mysql_query("SELECT * FROM formats WHERE type='".$me["type"]."';");
while ($c=mysql_fetch_array($r)) {
  $hasdv=false;
  if ($c["name"]=="Original") {
    echo "<li><a href=\"get.php?id=".$id."&format=".$c["id"]."\">".$c["name"]."</a> (".format_size(filesize("files/".$me["filename"])).")</li>\n";    
  } else {
    if (file_exists("formats/".$c["id"]."/$id") && ($s=filesize("formats/".$c["id"]."/$id"))>0) {
      echo "<li><a href=\"get.php?id=".$id."&format=".$c["id"]."\">".$c["name"]."</a> (".format_size($s).")</li>\n";
      if ($c["id"]==14) $hasdv=true;
    } else {
      echo "<li>".$c["name"]." ("."formats/".$c["id"]."/$id"." non disponible)</li>\n";
    }
  }
 }

if ($hasdv || true) {
?>
<?php

    $s=mq("SELECT * FROM tc WHERE media='".$me["id"]."' ORDER BY start;");
  if (mysql_num_rows($s)) {
    echo "<p><b>Liste des timecodes pour cette video (les lien permettent soit de t�l�charger le .DV, soit de le voir sur le moniteur) : </b></p>\n";
    echo "<table class=\"formv\"><th>DV</th><th>Moniteur</th><th>description</th><th>tags</th></tr>\n";
    $odd="odd";
    while ($d=mysql_fetch_array($s)) {
      if ($odd=="odd") $odd="even"; else $odd="odd";
      echo "<tr class=\"$odd\"><td style=\"width: 120px\">";
      echo "<a href=\"get.php?id=".$me["id"]."&action=extractdv&minstart=".(substr($d["start"],3,2)+substr($d["start"],0,2)*60)."&secstart=".substr($d["start"],6,2)."&minstop=".(substr($d["end"],3,2)+substr($d["end"],0,2)*60)."&secstop=".substr($d["end"],6,2)."\">".$d["start"]." � ".$d["end"]."</a>";
      echo "</td><td>";
      echo "<a href=\"view.php?id=".$me["id"]."&tc=".(substr($d["start"],3,2)*60+substr($d["start"],0,2)*3600+substr($d["start"],6,2))."\" target=\"view\">".$d["start"]."</a>";
      echo "</td><td> ".$d["description"]."</td><td> ";
      $e=mqlistone("SELECT t.name FROM tag t, tctag tt WHERE tt.tag=t.id AND tt.tc='".$d["id"]."';");
      if (count($e)) {
	echo "<i>".implode($e,",")."</i>";
      }
      echo "</td></tr>";
    }
    echo "</table>\n";
    echo "<p><b>Vous pouvez aussi demander un extrait particulier : </b></p>";
  }

?>
  <p>Pour extraire une partie de la video, entrez le timecode de debut et de fin, puis cliquez sur telecharger l`extrait.</p>


<form method="get" action="get">
<input type="hidden" name="id" value="<?=$me["id"]; ?>" />
<input type="hidden" name="action" value="extractdv" />
<table>
<tr><th>Debut : </th>
<td>  <input type="text" name="minstart" value="" size="3" /> min </td>
<td>  <input type="text" name="secstart" value="" size="3" /> sec</td>
</tr>
<tr><th>Fin : </th>
<td>  <input type="text" name="minstop" value="" size="3" /> min </td>
<td>  <input type="text" name="secstop" value="" size="3" /> sec</td>
</tr><tr>
<td colspan="3"><input type="submit" name="go" value="Telecharger l'extrait DV" /></td>
</tr></table>
</form>

<?php
 }

?>


<?php
require_once("foot.php");
?>