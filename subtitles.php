<?php

if (!$_SERVER["REMOTE_USER"]) {
  echo "Not Allowed";
  exit();
 }

require_once("config.php");

$id=intval($_REQUEST["id"]);

$m=mqone("SELECT * FROM media WHERE id='$id';");

if (!$id || !$m) {
  $error[]="ID de media non fourni ou media non trouvé !";
  require_once("index.php");
  exit();
 }

if ($_POST["action"]=="addsrt") {
  // Add a srt to the media :
  $error="";
  $me=mqone("SELECT * FROM media WHERE id='$id';");
  if (!$me) {
    $error.="Le media précisé n'existe pas !<br />\n";
  }
  if (!$alang2fr[$_POST["lang"]] && !$_POST["sid"]) {
    $error.="Vous devez choisir une langue valide<br />\n";
  } else $lang=$_POST["lang"];
  
  if (!$error && is_uploaded_file($_FILES['srtfile']['tmp_name'])) {
    if ($_POST["sid"]) {
      $sid=intval($_POST["sid"]);
      $old=mqone("SELECT * FROM srt WHERE media='$id' AND id='$sid';");
      if (!$old) {
	$error="Didn't found the subtitle to overwrite, please check";
      } else {
	@unlink("srt/$sid");
      }
    } else {
      mq("INSERT INTO srt SET media='$id', lang='$lang', encoded=0, datec=NOW(), datem=NOW();");
      $sid=mysql_insert_id();
    }
    $uploadfile="srt/$sid";
    if (move_uploaded_file($_FILES['srtfile']['tmp_name'], $uploadfile)) {
      $error="";
      if ($_POST["sid"]) { // overwrite = reencode; ) 
	mq("UPDATE srt SET datem=NOW(), encoded=0 WHERE id='$sid';");
      }
    } else {
      $error.="Erreur lors de l'envoi<br />\n";
      mq("DELETE FROM srt WHERE id='$sid';");
    }
  }
}

if ($_REQUEST["go"]) {
  // We change something ...
  //  mq("UPDATE media SET filename='".asl($filename)."', title='".asl($title)."', description='".asl($_REQUEST["description"])."', datec='".asl($_REQUEST["datec"])."' WHERE id='$id';");
 } else {
  $fields=array("filename","description","datec","title");
  foreach($fields as $f) $_REQUEST[$f]=nasl($m[$f]);
 }

// Get the current sub list : 
$subs=mqlist("SELECT * FROM srt WHERE media='$id';");

require_once("head.php");

if ($error) {
?>
<div class="error"><?php echo $error; ?></div>
<?php } ?>

<iframe id="view" name="view" style="position: fixed; top: 30px; right: 30px; width: 500px; height: 450px; border: 2px inset blue; padding: 2px" src="view.php?id=<?=$id; ?>&size=small"></iframe>

<h2>Modification des soustitres de <?=$m["filename"]; ?></h2>

<table class="formh">
<tr><th colspan="3">Action</th><th>État</th><th>Langue</th><th>Date d'envoi</th></tr> <!-- ' -->
<?php

if (!count($subs)) {
echo "<tr><td colspan=\"6\" style=\"width: 400px; text-align: center\">Aucun sous-titre pour l'instant</td></tr>\n";
} else 
  foreach($subs as $c) {
    echo "<tr>";
    echo "<td><a href=\"#view.php?sub=".$c["id"]."\">Voir</a></td>";
    echo "<td><a href=\"subtitles.php?id=".$id."&action=upload&sid=".$c["id"]."\">Remplacer</a></td>";
    echo "<td><a href=\"subtitles.php?id=".$id."&action=del&sid=".$c["id"]."\" onclick=\"return confirm('Etes-vous sûr de vouloir supprimer ce sous-titre!');\">Effacer</a></td>";
    echo "<td>".$asubetat[$c["encoded"]]."</td>";
    echo "<td>".$alang2fr[$c["lang"]]."</td>";
    echo "<td>".date_my2fr($c["datem"],1)."</td>";
    echo "</tr>";
  }
?>
<tr>
</table>

<form method="post" action="subtitles.php?id=<?=$id; ?>"  enctype="multipart/form-data">
<p>Envoi d'un fichier SRT : 
<?php 
if ($_GET["action"]=="upload") { 
$old=mqone("SELECT * FROM srt WHERE id='".intval($_GET["sid"])."';");
echo "Remplacement des sous-titres <b>".$alang2fr[$old["lang"]]."</b>";
echo "<input type=\"hidden\" name=\"sid\" value=\"".intval($_GET["sid"])."\" />";
}
?></p>
<input type="hidden" name="action" value="addsrt" />
  <table class="formh">
<tr><th>Langue : </th><td>
<?php 
if ($_GET["action"]=="upload") {
echo "<b>".$alang2fr[$old["lang"]]."</b>";
} else {
?>
<select name="lang" id="lang"><?php eoption($alang2frsel); ?></select>
<?php
}  
?></td></tr>
<tr><th>Fichier  : </th><td><input type="file" name="srtfile" id="srtfile" /></td></tr>
<tr><td colspan="2"> <input type="submit" name="go" value="Envoyer ce fichier" /> </td></tr>
</form>


<?php
  require_once("foot.php");
?>