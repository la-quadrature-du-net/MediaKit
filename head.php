<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" dir="ltr">
<head>
    <link rel="icon" href="./favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <script type="text/javascript" src="component_script.js"></script>
  <script type="text/javascript" src="script_common.js"></script>
  <link rel="stylesheet" type="text/css" href="/style.css" />
  <link rel="stylesheet" type="text/css" href="/component_style.css" />
</head>
   <body<?=$bodytag; ?>>
<table style="width: 80%"><tr><td>
<a href="/"><img src="quadrature_logo.png" style="float: left"/></a>
<h1><img src="titre_fr.png" /></h1>
</td></tr>
</table>
